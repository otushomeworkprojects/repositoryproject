﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace RepositoryProject
{
    public partial class Lessons
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Homework { get; set; }
        public string ZoomUrl { get; set; }
        public long Speaker { get; set; }
        public string Results { get; set; }

        public virtual Users SpeakerNavigation { get; set; }
    }
}
