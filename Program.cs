﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace RepositoryProject
{
    class Program
    {
        static void Main(string[] args)
        {
            using otusContext db = new();
            var users = db.Users.Include(u => u.Role).ToList();
            Console.WriteLine("Users list:");
            Console.WriteLine($"Id\tFirstName\tSecondName\tMiddleName\tEmail\tPhone\tRole");
            foreach (var user in users)
            {
                var middleName = user.MiddleName is null ? "null" : user.MiddleName;
                var phone = user.Phone is null ? "null" : user.Phone;
                var email = user.Email is null ? "null" : user.Email;
                Console.WriteLine($"{user.Id}\t{user.FirstName}\t{user.SecondName}\t{middleName}\t{email}\t{phone}\t{user.Role.Title}");
            }

            var roles = db.Roles.ToList();
            Console.WriteLine("\nRoles list:");
            Console.WriteLine($"Id\tTitle\t");
            foreach (var role in roles)
            {
                Console.WriteLine($"{role.Id}\t{role.Title}\t");
            }

            var lessons = db.Lessons.Include(l => l.SpeakerNavigation).ToList();
            Console.WriteLine("\nLessons list:");
            Console.WriteLine($"Id\tTitle\tHomework\tSpeaker\tZoomUrl\tResults\tDescription");
            foreach (var lesson in lessons)
            {
                Console.WriteLine($"{lesson.Id}\t{lesson.Title}\t{lesson.Homework}\t{lesson.SpeakerNavigation.FirstName}\t{lesson.ZoomUrl}\t{lesson.Results}\t{lesson.Description}");
            }

            Console.WriteLine("\nДля создания новой роли введите название: ");          
            var newRole = new Roles { Title = Console.ReadLine() };

            db.Roles.Add(newRole);
            db.SaveChanges();

            Console.WriteLine("Роль успешно создана!");
        }
    }
}
