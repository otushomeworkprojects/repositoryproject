﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace RepositoryProject
{
    public partial class Users
    {
        public Users()
        {
            Lessons = new HashSet<Lessons>();
        }

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string? MiddleName { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public long RoleId { get; set; }

        public virtual Roles Role { get; set; }
        public virtual ICollection<Lessons> Lessons { get; set; }
    }
}
